import { forty } from './module'

function f() {
    return forty() + 2
}

export { f }
