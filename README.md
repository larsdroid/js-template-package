# js-template-package

A template to be used as a **JavaScript NPM package**.  
Can be used to create a package that is used as a dependency (or devDependency)
of another JavaScript package or application. In other words, this can be used
to implement your own Node module in JavaScript.

## Included in this template

* NPM configuration (basically, the contents of our old trusty `package.json`)
* Vitest setup (tests are executed using `npm test`)
* Babel config to produce broadly supported JavaScript code in `dist`. Can be removed, if needed.
